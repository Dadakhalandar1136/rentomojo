import React from "react";
import "./Furnitures.css";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class Furniture extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            categoryProduct: "",
            isItemFound: false,
        }
    }

    handleClick = (type) => {
        let item = this.props.items.filter((product) => {
            let categoryItem = type.toLowerCase();
            let productName = product.category.toLowerCase();

            return productName === categoryItem;
        })
        if (item.length > 0) {
            this.setState({
                categoryProduct: item,
                isItemFound: true,
            })
        } else {
            this.setState({
                isItemFound: false
            })
        }
    }

    render() {
        return (
            <div className="row d-flex flex-wrap">
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/furniture-on-rent/bed">
                        <img src="https://www.rentomojo.com/public/images/category/furniture-category-filter/Beds.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("Bed") }}>Beds</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/furniture-on-rent/mattresses">
                        <img src="https://www.rentomojo.com/public/images/category/furniture-category-filter/mattress_v1.jpg" className="card-img-top" alt="mattress" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("Mattresses") }}>Mattresses</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/furniture-on-rent/wardrobe">
                        <img src="https://www.rentomojo.com/public/images/category/furniture-category-filter/Wardrobes_and_Organiser.jpg" className="card-img-top" alt="wardrobes" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("Wardrobe & Organizer") }}>Wardrobe & Organizer</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <img src="https://www.rentomojo.com/public/images/category/furniture-category-filter/Chest_of_Drawer.jpg" className="card-img-top" alt="drawer" id="furniture-image" />
                    <div className="card-body text-dark" id="card-body-search">
                        <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("Chest of Drawers") }}>Chest of Drawers</h5>
                    </div>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <img src="https://www.rentomojo.com/public/images/category/furniture-category-filter/Bedside_Table.jpg" className="card-img-top" alt="drawer" id="furniture-image" />
                    <div className="card-body text-dark" id="card-body-search">
                        <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("Beside Tables") }}>Beside Tables</h5>
                    </div>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <img src="https://www.rentomojo.com/public/images/category/furniture-category-filter/Sofas.jpg" className="card-img-top" alt="drawer" id="furniture-image" />
                    <div className="card-body text-dark" id="card-body-search">
                        <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("Sofas") }}>Sofas</h5>
                    </div>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <img src="https://www.rentomojo.com/public/images/category/furniture-category-filter/Day_Bed.jpg" className="card-img-top" alt="drawer" id="furniture-image" />
                    <div className="card-body text-dark" id="card-body-search">
                        <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("Sofa Bed & Day Bed") }}>Sofa Bed & Day Bed</h5>
                    </div>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <img src="https://www.rentomojo.com/public/images/category/furniture-category-filter/Recliner.jpg" className="card-img-top" alt="drawer" id="furniture-image" />
                    <div className="card-body text-dark" id="card-body-search">
                        <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("Recliner and Rocker") }}>Recliner and Rocker</h5>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.productData.items,
    }
}

export default connect(mapStateToProps)(Furniture);