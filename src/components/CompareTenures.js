import React from "react";

class CompareTenures extends React.Component {

    render() {
        return (
            <div className="modal fade" id="exampleModal5" tabIndex="0" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-lg bg-white" style={{ minWidth: "60rem" }}>
                    <div className="d-flex" style={{ minWidth: "60rem" }}>
                        <div className="modal-content d-flex border border-0">
                            <button type="button" className="btn-close ms-auto pt-3 pe-3" data-bs-dismiss="modal" aria-label="Close"></button>
                            <div className="modal-body d-flex ms-5 mt-2">
                                <div className="pt-4 lh-lg" style={{ minWidth: "16rem" }}>
                                    <h6 className="mb-0">Compare all</h6>
                                    <h6 className="mb-0">rental</h6>
                                    <h6 className="mb-2">tenures</h6>
                                    <div className="px-2 pt-1 d-flex mb-3" style={{ backgroundColor: "#f1f1f1" }}>
                                        <img style={{ width: "1rem" }} src="https://www.rentomojo.com/public/images/product/compare-tenure/savings-on-monthly-rental.svg" alt="tenureOne" />
                                        <p className="mb-0 ps-1">Saving on monthly rental</p>
                                    </div>
                                    <div className="px-2 pt-1 d-flex mb-3">
                                        <img style={{ width: "1rem" }} src="https://www.rentomojo.com/public/images/product/compare-tenure/early-closure-charge.svg" alt="tenureTwo" />
                                        <p className="mb-0 ps-1">Early closure charge</p>
                                    </div>
                                    <div className="px-2 pt-1 d-flex mb-3" style={{ backgroundColor: "#f1f1f1" }}>
                                        <img style={{ width: "1rem" }} src="https://www.rentomojo.com/public/images/product/compare-tenure/free-relocation.svg" alt="tenureThree" />
                                        <p className="mb-0 ps-1">Free relocation</p>
                                    </div>
                                    <div className="px-2 pt-1 d-flex mb-3">
                                        <img style={{ width: "1rem" }} src="https://www.rentomojo.com/public/images/product/compare-tenure/free-upgrade.svg" alt="tenureFour" />
                                        <p className="mb-0 ps-1">Free upgrade</p>
                                    </div>
                                </div>
                                <div className="d-flex">
                                    <div className="pt-4 border border-end-0 d-flex flex-column align-items-center lh-lg" style={{ width: "12rem" }}>
                                        <p className="mb-0" style={{ fontSize: "0.9rem" }}>Min 3 month</p>
                                        <h6 className="mb-3">₹{this.props.product.price?.threeMonths}/mo</h6>
                                        <p className="px-2 pt-1 w-100 text-center" style={{ backgroundColor: "#f1f1f1", color: "#fdb614" }}>0%</p>
                                        <p className="px-2 pt-1 " style={{ color: "#1dbdc0" }}>1 Months's rent</p>
                                        <p className="px-2 pt-1 w-100 text-center" style={{ backgroundColor: "#f1f1f1" }}>After 6 months</p>
                                        <p className="px-2 pt-1 " >After 36 months</p>
                                        <button className="btn mb-3 rounded-1" type="submit" style={{ backgroundColor: "#1dbdc0", width: "6rem", cursor: "pointer" }}>Select</button>
                                    </div>
                                    <div className="pt-4 border border-end-0 d-flex flex-column align-items-center lh-lg" style={{ width: "12rem" }}>
                                        <p className="mb-0" style={{ fontSize: "0.9rem" }}>Min 6 month</p>
                                        <h6 className="mb-3">₹{this.props.product.price?.sixMonths}/mo</h6>
                                        <p className="px-2 pt-1 w-100 text-center" style={{ backgroundColor: "#f1f1f1", color: "#1dbdc0" }}>10%</p>
                                        <p className="px-2 pt-1 " >1.25 Month's rent</p>
                                        <p className="px-2 pt-1 w-100 text-center" style={{ backgroundColor: "#f1f1f1" }}>After 6 months</p>
                                        <p className="px-2 pt-1 " >After 36 months</p>
                                        <button className="btn mb-3 rounded-1" type="submit" style={{ backgroundColor: "#1dbdc0", width: "6rem", cursor: "pointer" }}>Select</button>
                                    </div>
                                    <div className="pt-4 border me-3 d-flex flex-column align-items-center lh-lg" style={{ width: "12rem" }}>
                                        <p className="mb-0" style={{ fontSize: "0.9rem" }}>Min 12 month</p>
                                        <h6 className="mb-3">₹{this.props.product.price?.twelveMonths}/mo</h6>
                                        <p className="px-2 pt-1 w-100 text-center" style={{ backgroundColor: "#f1f1f1", color: "#1dbdc0" }}>20%</p>
                                        <p className="px-2 pt-1 " >1.5 Month's rent</p>
                                        <p className="px-2 pt-1 w-100 text-center" style={{ backgroundColor: "#f1f1f1" }}>After 6 months</p>
                                        <p className="px-2 pt-1 " >After 36 months</p>
                                        <button className="btn mb-3 rounded-1" type="submit" style={{ backgroundColor: "#1dbdc0", width: "6rem", cursor: "pointer" }}>Select</button>
                                    </div>
                                </div>
                            </div>
                            <div className="ms-5 me-5 pt-3 ps-2 mb-4 mt-4" style={{ border: "dashed gray 0.1rem", fontSize: "0.7rem", color: "gray" }}>
                                <ol>
                                    <li>Once your minimum rental period is over, you can continue to rent the product at the same monthly charge.</li>
                                    <li>In case you wish to return the product before your plan's minimum rental tenure, you will have to pay a small early closure charge.</li>
                                    <li>You can increase your minimum tenure and avail further savings on your monthly rent anytime from your customer dashboard.</li>
                                    <li>You might need to submit a few KYC documents before we initiate your product’s delivery.</li>
                                    <li>Your monthly rent is post-paid and your bill is generated on the 1st of every month. Your rent is always calculated on a daily basis.</li>
                                </ol>
                            </div>
                            <button type="button" className="btn btn-danger ms-auto me-5 rounded-1 mb-2" style={{ width: "8rem" }} data-bs-dismiss="modal" aria-label="Close">Confirm</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CompareTenures;