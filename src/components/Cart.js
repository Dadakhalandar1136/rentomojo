import React from "react";
import "./Cart.css";
import { connect } from "react-redux";

class Cart extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            depositSum: 0,
            priceSum: 0,
            proceedSuccess: "",
        }
    }

    increamentProduct = (product, productId) => {
        this.setState((prevState) => {
            return {
                ...prevState,
                depositSum: prevState.depositSum + product.deposit,
                priceSum: prevState.priceSum + product.price.threeMonths
            }
        })
        this.props.increamentQuantity(productId);
    }

    decrementProduct = (product, productId) => {
        if (this.props.productsBuy[0].quantity > 1) {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    depositSum: prevState.depositSum - product.deposit,
                    priceSum: prevState.priceSum - product.price.threeMonths
                }
            })
            this.props.decrementQuantity(productId);
        }
    }

    deleteProduct = (productId) => {
        this.props.deleteProductFromCart(productId);
        setTimeout(() => {
            this.toTotalPriceUpdate();
        }, 300);
    }

    toTotalPriceUpdate = () => {
        let allSumPrice = 0;
        this.props.productsBuy.map((eachItem) => {
            return allSumPrice += eachItem.price.threeMonths * eachItem.quantity
        })
        this.setState({
            priceSum: allSumPrice,
            depositSum: allSumPrice,
        })
    }

    handleProceed = () => {
        this.setState({
            proceedSuccess: "Awesome!"
        })
        setTimeout(() => {
            this.setState({
                proceedSuccess: ""
            })
            window.location.reload();
        }, 4000)
    }

    componentDidMount() {
        let totalDeposit = this.props.productsBuy.reduce((sum, currentItem) => {
            return sum + (currentItem.deposit * currentItem.quantity);
        }, 0)

        let totalPrice = this.props.productsBuy.reduce((sum, currentItem) => {
            return sum + (currentItem.price.threeMonths * currentItem.quantity);
        }, 0)

        this.toTotalPriceUpdate()

        this.setState({
            depositSum: totalDeposit,
            priceSum: totalPrice,
        })
    }

    render() {
        return (
            <div className="cart-main-container">
                <div className="sub-cart-main-container">
                    <div className="left-column">
                        <div className="add-on">
                            <div className="left-add-on">
                                <div className="add-on-list">
                                    <h2>Suggested Addons</h2>
                                    <img src="https://p.rmjo.in/productSquare/bqema5lu-250x250.jpg" alt="addOn" />
                                </div>
                                <div className="add-on-list-two">
                                    <p>0 of 1 added</p>
                                    <p>These addons are curated to give the best experience with the products in your cart.</p>
                                </div>
                                <div className="chooseAdd">
                                    <button type="button" className="btn btn-danger">Choose & Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="billing-container">
                        <h6>Order Summary</h6>
                        <div className="sub-billing-container">
                            <div className="payble-box">
                                <div className="sub-payble-box">
                                    <div className="refundable-box">
                                        <p>Refundable Deposit</p>
                                        <p>₹{this.state.depositSum}</p>
                                    </div>
                                    <div className="delivery-box">
                                        <p>Delivery Charges</p>
                                        <p>{this.props.productsBuy[0]?.delivery}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="payble-monthly">
                                <div className="sub-payble-monthly">
                                    <div className="products-rent-box">
                                        <p>Products Rent</p>
                                        <p>₹{this.state.priceSum}/mo</p>
                                    </div>
                                    <div className="products-rent-box">
                                        <p>GST</p>
                                        <p>₹{Math.round((this.state.priceSum * 18) / 100)}/mo</p>
                                    </div>
                                    <hr />
                                    <div className="products-rent-box">
                                        <p>Total</p>
                                        <p>₹{(this.state.priceSum) + Math.round((this.state.priceSum * 18) / 100)}/mo</p>
                                    </div>
                                </div>
                                <div className="pay-post ps-2">
                                    <img src="https://www.rentomojo.com/public/images/radical-cart/icons/calendar__icon.svg" alt="calender" />
                                    <p className="pt-3 ps-1">Not to be paid now. Pay post usage every month.</p>
                                </div>
                            </div>
                        </div>
                        <div className="payment-container">
                            <button
                                type="button"
                                className="btn btn-danger mt-3"
                                data-bs-toggle={this.state.depositSum ? "offcanvas" : ""}
                                data-bs-target="#offcanvasRight"
                                aria-controls="offcanvasRight"
                                onClick={() => {
                                    return this.handleProceed()
                                }}>
                                <div className="sub-button-content">
                                    <h6>₹{this.state.depositSum}</h6>
                                    <p>Payable Now</p>
                                </div>
                                <h6>Proceed</h6>
                            </button>
                            <div className="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
                                <div className="offcanvas-header" id="payment-heading">
                                    <h5 className="offcanvas-title" id="offcanvasRightLabel">Choose payment mode</h5>
                                    <button type="button" className="btn-close w-25" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                                </div>
                                <div className="offcanvas-body overflow-hidden">
                                    <div>
                                        <h6>Total Amount Payable</h6>
                                        <h5>₹{this.state.depositSum}</h5>
                                    </div>
                                    <div className="condition-box">
                                        <input type="checkbox" checked />
                                        <p>I hereby agree to RentoMojo terms and conditions.</p>
                                    </div>
                                    <h6>Net Banking, Debit Card & Credit Card</h6>
                                    <div className="banking-box d-flex flex-row align-items-center p-3">
                                        <img src="https://www.rentomojo.com/public/images/cart/payment/razorpay.png" alt="razorpay" className="w-25" />
                                        <p className="mb-0">Credit card / Debit card / Netbanking</p>
                                        <i class="fa-solid fa-angle-right"></i>
                                    </div>
                                    <h6 className="pt-2">Wallets</h6>
                                    <div className="banking-box mb-1 d-flex flex-row align-items-center p-3">
                                        <div className="d-flex">
                                            <img src="https://www.rentomojo.com/public/images/cart/payment/paytm.png" alt="paytm" className="w-25 h-25" />
                                            <p className="mb-0">Pay via Paytm</p>
                                        </div>
                                        <i class="fa-solid fa-angle-right"></i>
                                    </div>
                                    <div className="banking-box mb-2 d-flex flex-row align-items-center p-3">
                                        <div className="d-flex">
                                            <img src="https://www.rentomojo.com/public/images/cart/payment/phonepe.png" alt="phonepe" className="w-25 h-25" />
                                            <p className="mb-0">Pay via PhonePe</p>
                                        </div>
                                        <i className="fa-solid fa-angle-right"></i>
                                    </div>
                                    <button
                                        type="button"
                                        className="btn btn-outline-danger w-100 fs-5 d-flex justify-content-center"
                                        onClick={() => this.handleProceed()}
                                        data-bs-toggle="modal" data-bs-target="#exampleModal3"
                                    >
                                        Continue
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="right-column">
                    <div className="coupon-container">
                        <img src="https://www.rentomojo.com/public/images/radical-cart/icons/coupon__icon.svg" alt="dicount" />
                        <h6 className="ps-2 pb-2">Have a coupon code?</h6>
                    </div>
                    {this.props.productsBuy.map((product) => {
                        return (
                            <div key={product.id} className="">
                                <div className="card-add-box">
                                    <div className="card-product-box-top">
                                        <div className="card-img-box">
                                            <img src={product.image} alt={product.title} />
                                        </div>
                                        <div className="card-details-box">
                                            <div className="card-title-box d-flex justify-content-between" style={{ width: "12rem" }}>
                                                <div>
                                                    <h6>{product.title}</h6>
                                                </div>
                                                <i className="fa-solid fa-trash-can" onClick={() => {
                                                    return this.deleteProduct(product.id)
                                                }}></i>
                                            </div>
                                            <div className="price-content d-flex flex-row">
                                                <div>
                                                    <p className="rent-box mb-0">Rent</p>
                                                    <h6>₹{((product.price.threeMonths) * (product.quantity))}</h6>
                                                </div>
                                                <div>
                                                    <p className="rent-box mb-0 ps-5">Deposit</p>
                                                    <h6 className="ps-5">₹{((product.deposit) * (product.quantity))}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="cart-quantity-container">
                                        <div className="quantity-set">
                                            <i className="fa-solid fa-minus" onClick={() => {
                                                return this.decrementProduct(product, product.id)
                                            }}></i>
                                            <p className="mt-3">{product.quantity}</p>
                                            <i className="fa-regular fa-plus" onClick={() => {
                                                return this.increamentProduct(product, product.id)
                                            }}></i>
                                        </div>
                                        <div className="dropdown">
                                            <a className="btn btn-white dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" id="select-months-box">
                                                12 Months
                                            </a>
                                            <ul className="dropdown-menu ">
                                                <li><a className="dropdown-item" href="#">12 Months</a></li>
                                                <li><a className="dropdown-item" href="#">6 Months</a></li>
                                                <li><a className="dropdown-item" href="#">3 Months</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })
                    }
                </div>
                {
                    this.state.proceedSuccess &&
                    <div class="modal" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header border-0 d-flex flex-column">
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/Eo_circle_green_checkmark.svg/2048px-Eo_circle_green_checkmark.svg.png"
                                        alt="tick"
                                        style={{width: "8rem", height: "8rem", marginTop: "-5rem", backgroundColor: "white", borderRadius: "5rem"}}
                                    />
                                    <h1 class="modal-title fs-4" id="exampleModalLabel">{this.state.proceedSuccess}</h1>
                                    <h6>Your booking has been confirmed</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        productsBuy: state.cartReducer.list
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        increamentQuantity: (productId) => dispatch({
            type: "INCREAMENT_QUANTITY",
            payload: productId
        }),

        decrementQuantity: (productId) => dispatch({
            type: "DECREMENT_QUANTITY",
            payload: productId
        }),

        deleteProductFromCart: (productId) => dispatch({
            type: "DELETE_PRODUCT_FROM_CART",
            payload: productId
        })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);