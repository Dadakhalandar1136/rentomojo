import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class Fitness extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            categoryProduct: "",
            isItemFound: false,
        }
    }

    handleClick = (type) => {
        let item = this.props.items.filter((product) => {
            let categoryItem = type.toLowerCase();
            let productName = product.category.toLowerCase();

            return productName === categoryItem;
        })
        // console.log(item);
        if (item.length > 0) {
            this.setState({
                categoryProduct: item,
                isItemFound: true,
            })
        } else {
            this.setState({
                isItemFound: false
            })
        }
    }
    
    render() {
        // console.log(this.state.categoryProduct);
        return (
            <div className="row d-flex flex-wrap">
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/fitness-on-rent/threadmills">
                        <img src="https://www.rentomojo.com/public/images/category/fitness/treadmills.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("threadmills") }}>Treadmills</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/fitness-on-rent/crosstrainers">
                        <img src="https://www.rentomojo.com/public/images/category/fitness/cross-trainers.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("crosstrainers") }}>Cross Trainers</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/fitness-on-rent/smartphones">
                        <img src="https://www.rentomojo.com/public/images/category/fitness/exercise-bikes.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("smartphones") }}>Exircise Bikes</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/fitness-on-rent/smartphones">
                        <img src="https://www.rentomojo.com/public/images/category/fitness/massagers.png" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("smartphones") }}>Massagers</h5>
                        </div>
                    </Link>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.productData.items,
    }
}

export default connect(mapStateToProps)(Fitness);