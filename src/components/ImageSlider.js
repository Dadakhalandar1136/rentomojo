import React from "react";
import slideImageOne from "../assets/slideImageOne.jpg";
import slideImageTwo from "../assets/slideImageTwo.jpg";
import slideImageThree from "../assets/slideImageThree.jpg";
import slideImageFour from "../assets/slideImageFour.jpg";
import './ImageSlider.css';

class ImageSlider extends React.Component {
    render() {
        return (
            <div id="carouselExampleInterval" className="carousel slide w-75" data-bs-ride="carousel">
                <div className="carousel-inner">
                    <div className="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="3" aria-label="Slide 4"></button>
                    </div>
                    <div className="carousel-item active" data-interval="3000">
                        <img src={slideImageOne} className="d-block w-100" alt="..." />
                    </div>
                    <div className="carousel-item" data-interval="3000">
                        <img src={slideImageTwo} className="d-block w-100" alt="..." />
                    </div>
                    <div className="carousel-item" data-interval="3000">
                        <img src={slideImageThree} className="d-block w-100" alt="..." />
                    </div>
                    <div className="carousel-item" data-interval="3000">
                        <img src={slideImageFour} className="d-block w-100" alt="..." />
                    </div>
                </div>
                <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Previous</span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Next</span>
                </button>
                <div className="slider-know-more">                         
                    <span className="form-control pt-5" type="text" aria-label="Disabled input example" disabled >
                    <i className="fa-solid fa-virus-covid pe-1"></i>Safety precautions during COVID-19. We’re taking additional steps and precautionary measures to protect our community from COVID-19.
                    <a href="#" className="know-more">Know More</a><i className="fa-solid fa-circle-chevron-right"></i>
                    </span>
                </div>
            </div>
        )
    }
}

export default ImageSlider;