import React from "react";
import doubleBed from "../assets/double-bed.png";
import location from "../assets/location.png";
import maintenance from "../assets/maintenance.png";
import calcelSeat from "../assets/seat.png";
import doubleBedTwo from "../assets/double-bed-two.png";
import update from "../assets/update.png";

import "./RentingDetails.css";

class RentingDetails extends React.Component {
    render() {
        return (
            <div id="renting-main-container">
                <div className="column">
                    <div className="card-container">
                        <img src={doubleBed} className="card-image" alt="..." />
                        <div className="card-body-box">
                            <h5 className="card-title-name">Finest-quality products</h5>
                            <p className="card-text-space">Quality matters to you, and us! That's why we do a strict quality-check for every product.</p>
                        </div>
                    </div>
                </div>
                <div className="column">
                    <div className="card-container">
                        <img src={location} className="card-image" alt="..." />
                        <div className="card-body-box">
                            <h5 className="card-title-name">Free relocation</h5>
                            <p className="card-text-space">Changing your house or even your city? We'll relocate your rented products for free.</p>
                        </div>
                    </div>
                </div>
                <div className="column">
                    <div className="card-container">
                        <img src={maintenance} className="card-image" alt="..." />
                        <div className="card-body-box">
                            <h5 className="card-title-name">Free maintenance</h5>
                            <p className="card-text-space">Keeping your rented products in a spick and span condition is on us, so you can sit back and relax.</p>
                        </div>
                    </div>
                </div>
                <div className="column">
                    <div className="card-container">
                        <img src={calcelSeat} className="card-image" alt="..." />
                        <div className="card-body-box">
                            <h5 className="card-title-name">Cancel anytime</h5>
                            <p className="card-text-space">Pay only for the time you use the product and close your subscription without any hassle.</p>
                        </div>
                    </div>
                </div>
                <div className="column">
                    <div className="card-container">
                        <img src={doubleBedTwo} className="card-image" alt="..." />
                        <div className="card-body-box">
                            <h5 className="card-title-name">Easy return on delivery</h5>
                            <p className="card-text-space">If you don't like the product on delivery, you can return it right away—no questions asked.</p>
                        </div>
                    </div>
                </div>
                <div className="column">
                    <div className="card-container">
                        <img src={update} className="card-image" alt="..." />
                        <div className="card-body-box">
                            <h5 className="card-title-name">Keep upgrading</h5>
                            <p className="card-text-space">Bored of the same product? Upgrade to try another, newer design and enjoy the change!</p>
                        </div>
                    </div>
                </div>
                <div>
                    <h5 className="know-more-option">Know More</h5>
                </div>
            </div>
        )
    }
}

export default RentingDetails;