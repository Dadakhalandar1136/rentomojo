import React from "react";

class MostPopularProducts extends React.Component {

    render() {

        return (
            <div className="row">
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <img src="https://p.rmjo.in/productSquare/djbfgoay-500x500.jpg" className="card-img-top" id="card-top-image" alt="" />
                    <div className="card-body" id="card-body-search">
                        <h5 className="card-title" id="title-search-text">Betty Dresser with Stool</h5>
                    </div>
                    <ul className="list-group list-group-flush">
                        <p className="card-text" id="card-text-search">$199/mo</p>
                    </ul>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <img src="https://p.rmjo.in/productSquare/djbfgoay-500x500.jpg" className="card-img-top" id="card-top-image" alt="" />
                    <div className="card-body" id="card-body-search">
                        <h5 className="card-title" id="title-search-text">Betty Dresser with Stool</h5>
                    </div>
                    <ul className="list-group list-group-flush">
                        <p className="card-text" id="card-text-search">$199/mo</p>
                    </ul>
                </div>
            </div>
        )
    }
}

export default MostPopularProducts;