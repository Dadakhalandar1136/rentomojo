import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class Appliances extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            categoryProduct: "",
            isItemFound: false,
        }
    }

    handleClick = (type) => {
        let item = this.props.items.filter((product) => {
            let categoryItem = type.toLowerCase();
            let productName = product.category.toLowerCase();

            return productName === categoryItem;
        })
        console.log(item);
        if (item.length > 0) {
            this.setState({
                categoryProduct: item,
                isItemFound: true,
            })
        } else {
            this.setState({
                isItemFound: false
            })
        }
    }
    
    render() {
        return (
            <div className="row d-flex flex-wrap">
            <div>
                <h2>Browse by Furniture type</h2>
            </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/appliances-on-rent/washing-machine">
                        <img src="https://www.rentomojo.com/public/images/category/appliances-bg/washing-machines.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("washing-machine") }}>Washing Machines</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/appliances-on-rent/washing-machine">
                        <img src="https://www.rentomojo.com/public/images/category/appliances-bg/refrigerators-and-freezers.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("washing-machine") }}>Refrigerators & Freezers</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/appliances-on-rent/washing-machine">
                        <img src="https://www.rentomojo.com/public/images/category/appliances-bg/televisions.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("washing-machine") }}>Televisions</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/appliances-on-rent/washing-machine">
                        <img src="https://www.rentomojo.com/public/images/category/appliances-bg/air-conditioners.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("washing-machine") }}>Air Conditioners</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/appliances-on-rent/washing-machine">
                        <img src="https://www.rentomojo.com/public/images/category/appliances-bg/air-purifiers.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("washing-machine") }}>Water & Air Purifiers</h5>
                        </div>
                    </Link>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.productData.items,
    }
}

export default connect(mapStateToProps)(Appliances);