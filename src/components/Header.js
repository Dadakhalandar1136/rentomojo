import React from "react";

import logo from '../assets/app-logo.png';
import { Link } from "react-router-dom";
import "./Header.css";
import LoginModal from "./LoginModal";
import Cities from "./Cities";
import { connect } from "react-redux";



class Header extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
        }
    }

    handleHover = () => {
        this.setState({
            isOpen: true,
        })
    }

    handleLeave = () => {
        this.setState({
            isOpen: false
        })
    }

    handleItemClick = () => {
        this.setState({
            isOpen: false,
        })
    }

    handleClick = (event) => {
        event.preventDefault();
    }

    handleItemLogout = () => {
        setTimeout(() => {
            window.location.reload();
        }, 10)
    }

    render() {
        return (
            <div className="container">
                <div className=" d-flex flex-row align-items-center justify-content-evenly">
                    <nav className="navbar ">
                        <div className="container-fluid">
                            <Link to="/">
                                <a className="navbar-brand" href="/">
                                    <img src={logo} alt="Logo" width="140" height="45" className="d-inline-block align-text-top" />
                                </a>
                            </Link>
                        </div>
                    </nav>
                    <nav className="navbar navbar-expand-lg navbar-white bg-white">
                        <div className="container-fluid">
                            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDarkDropdown" aria-controls="navbarNavDarkDropdown" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
                                <ul className="navbar-nav">
                                    <li className="nav-item dropdown">
                                        <button className="btn btn-white dropdown-toggle" aria-expanded="false" data-bs-toggle="modal" data-bs-target="#exampleModal1">
                                            Choose City
                                        </button>
                                        <Cities />
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <nav className="navbar" >
                        <div className="container-fluid">
                            <form className="d-flex" role="search" >
                                <input className="form-control me-2 pe-3" type="search" placeholder="Search" aria-label="Search"
                                    onChange={(event) => this.props.handleChangeValue(event)}
                                />
                                <Link to="/searched">
                                    <div className="search-button">
                                        <button className="btn btn-outline-white" type="submit"><i className="fa-solid fa-magnifying-glass p-2"></i></button>
                                    </div>
                                </Link>
                            </form>
                        </div>
                    </nav>
                    <Link to="/cart">
                        <nav className="navbar pe-4 text-black">
                            <div className="d-flex pe-1">
                                <i className="fa-solid fa-cart-shopping"></i>
                            </div>
                            Cart
                        </nav>
                    </Link>

                    {this.props.user.userName ?
                        <div className="d-flex flex-column">
                            <div className="dropdown">
                                <button
                                    type="button"
                                    className="btn btn-outline-danger bg-white text-dark"
                                    style={{ width: "9rem" }}
                                    onMouseEnter={this.handleHover}
                                    onMouseDown={this.handleLeave}>
                                    {this.props.user.userName}
                                    {this.state.selectedItem}
                                </button>
                                {
                                    this.state.isOpen && (
                                        <div className="dropdown position-absolute border bg-white mb-2 mt-2" style={{ zIndex: "5" }}>
                                            <ul className="px-4 pt-2 lh-lg" style={{width: "10rem"}}>
                                                <Link to="/myaccount">
                                                    <li
                                                        style={{ cursor: "pointer", listStyle: "none", fontSize: "0.9rem", color: "gray" }}
                                                    >
                                                        <i class="fa-solid fa-user pe-3 pb-3"></i>
                                                        My Account
                                                    </li>
                                                </Link>
                                                <li
                                                    style={{ cursor: "pointer", listStyle: "none", fontSize: "0.9rem", color: "gray" }}
                                                >
                                                    <i class="fa-solid fa-heart pe-3 pb-3"></i>
                                                    My Wishlist
                                                </li>
                                                <li
                                                    style={{ cursor: "pointer", listStyle: "none", color: "gray", fontSize: "0.9rem" }}
                                                >
                                                    <i class="fa-solid fa-gear pe-3 pb-3"></i>
                                                    Settings
                                                </li>
                                                <Link to="/">
                                                    <li
                                                        style={{ cursor: "pointer", listStyle: "none", color: "gray", fontSize: "0.9rem" }}
                                                        onClick={() => this.handleItemLogout()}
                                                    >
                                                        <i class="fa-solid fa-power-off pe-3 pb-3"></i>
                                                        Logout
                                                    </li>
                                                </Link>
                                            </ul>
                                        </div>
                                    )
                                }
                            </div>
                        </div>
                        :
                        <button type="button" className="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModal">LOGIN/SIGNUP</button>
                    }
                    <LoginModal />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.userReducer.user
    }
}

export default connect(mapStateToProps)(Header);