import React from "react";

import "./ProductsList.css";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class ProductList extends React.Component {
    render() {
        return (
            <>
                <div className="product-list-container">
                    {
                        this.props.productsList.map((product) => {
                            return (
                                <div className="d-flex flex-column">
                                    <div className="card" id="card-container" key={product.id}>
                                        <Link to={`${product.id}`}>
                                            <img src={product.image} className="card-img-top" alt={product.title} />
                                            <div className="card-body" id="card-body-conatiner">
                                                <h4 className="m-0 pb-3 text-dark" style={{ fontSize: "0.8rem", textOverflow: "ellipsis", whiteSpace: "nowrap", overflow: "hidden" }}>{product.title}</h4>
                                                <div className="see-more-container">
                                                    <div>
                                                        <p className="card-text mb-0 text-dark" style={{ fontSize: "0.7rem" }}>Rent</p>
                                                        <span className="text-dark">₹{product.price.threeMonths}/mo</span><br />
                                                    </div>
                                                    <button type="button" id="see-more-button" className="btn btn-outline-danger">See More</button>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        productsList: state.productData.items,
    }
}

export default connect(mapStateToProps)(ProductList);