import React from "react";
import "./Footer.css";

class Footer extends React.Component {
    render() {
        return (
            <div className="footer-container">
                <div className="main-container">
                    <div className="col-md-3 col-sm-3 col-xs-12 hidden-xs mt-1">
                        <p>
                            © 2023. Edunetwork Pvt. Ltd.
                        </p>
                    </div>
                    <div className="social-media d-flex flex-row w-25">
                        <a>
                            <div className="facebook">
                                <i className="fa-brands fa-facebook-f ps-2" style={{ color: "#90949d" }}></i>
                            </div>
                        </a>
                        <a>
                            <div className="twitter">
                                <i className="fa-brands fa-twitter"></i>
                            </div>
                        </a>
                        <a>
                            <div className="linkedin">
                                <i className="fa-brands fa-linkedin"></i>
                            </div>
                        </a>
                        <a>
                            <div className="youtube">
                                <i className="fa-brands fa-youtube"></i>
                            </div>
                        </a>
                        <a>
                            <div className="instagram">
                                <i className="fa-brands fa-instagram"></i>
                            </div>
                        </a>
                        <a>
                            <div className="dribbble">
                                <i className="fa-brands fa-dribbble"></i>   
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        )
    }
}

export default Footer;