import React from "react";

class FooterDetails extends React.Component {
    render() {
        return (
            <div className="w-100 p-5 d-flex flex-column" style={{ backgroundColor: "hsl(196, 17%, 95%)" }}>
                <div>
                    <h6 className="ps-5">Embrace Your Dream Lifestyle With RentoMojo In Mumbai</h6>
                    <p className="ps-5" style={{ fontWeight: "300", fontSize: "0.9rem" }}>
                        RentoMojo is one of India’s best-known rental brands with a pan-India presence. You can rent furniture, appliances, and electronics from us in Mumbai for a monthly fee.
                        Instead of paying tens of thousands to purchase said items from a store, you can rent them from us at a discounted price.
                    </p>
                    <p className="ps-5" style={{ fontWeight: "300", fontSize: "0.9rem" }}>
                        RentoMojo provides very reasonable rental rates, allowing you to save a great deal of money. We offer convenient rental options, short-term and long-term. Our long-term plans offer maximum savings.
                        No matter what plan you sign up for, you receive added benefits from us such as free relocation, damage waiver, and free maintenance.
                    </p>
                    <p className="ps-5" style={{ fontWeight: "300", fontSize: "0.9rem" }}>
                        You’ll find top-rated, best-selling products in our inventory, made by the most dependable brands in the market. You can rent any item from us online in a matter of minutes. After we process your order, we will deliver it to you anywhere in Mumbai.
                        We offer fast delivery all over the city, including localities such as Lower Parel, Worli, Byculla, Bandra, and Santacruz.
                    </p>
                    <div className="d-flex justify-content-around pt-5">
                        <div className="d-flex flex-column">
                            <h6 className="pb-3">RENTOMOJO</h6>
                            <p>About Us</p>
                            <p>Culture</p>
                            <p>Investor</p>
                            <p>Careers</p>
                            <p>Contant</p>
                            <p>Our Benifits</p>
                            <p>Sitemap</p>
                        </div>
                        <div className="d-flex flex-column">
                            <h6 className="pb-3">INFORMATION</h6>
                            <p>Blog</p>
                            <p>FAQs</p>
                            <p>Documents Required</p>
                        </div>
                        <div className="d-flex flex-column">
                            <h6 className="pb-3">POLICIES</h6>
                            <p>Shipping Policy</p>
                            <p>Cancellation & Return</p>
                            <p>Privecy Policy</p>
                            <p>Rental Terms & Conditions</p>
                            <p>Referral Terms & Conditions</p>
                        </div>
                        <div className="d-flex flex-column">
                            <h6 className="pb-3">NEED HELP ?</h6>
                            <div className="border border-dark rounded-2 p-2 mb-3" style={{ cursor: "pointer" }}>
                                <p className="mb-0">Chat with us (9AM - 6PM)</p>
                            </div>
                            <div className="d-flex">
                                <i className="fa-regular fa-paper-plane mt-1 pe-3"></i>
                                <p>jo@rentomojo.com</p>
                            </div>
                            <h6>DOWNLOAD APP</h6>
                            <div>
                                <img className="pe-1" style={{ width: "6rem", cursor: "pointer" }} src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/2560px-Google_Play_Store_badge_EN.svg.png" alt="googleplay" />
                                <img style={{ width: "6rem", cursor: "pointer" }} src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/2560px-Download_on_the_App_Store_Badge.svg.png" alt="apple" />
                            </div>
                        </div>
                    </div>
                    <hr></hr>
                </div>
            </div>
        )
    }
}

export default FooterDetails;