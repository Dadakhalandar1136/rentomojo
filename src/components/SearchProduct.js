import React from "react";
import { Link } from "react-router-dom";
import "./SearchProduct.css";

class SearchProduct extends React.Component {
    render() {
        let product = this.props.item;
        return (
            <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                <Link to={`/${product.id}`}>
                    <img src={product.image} className="card-img-top" id="card-top-image" alt={product.title} />
                    <div className="card-body" id="card-body-search">
                        <h5 className="card-title" id="title-search-text">{product.title}</h5>
                    </div>
                    <ul className="list-group list-group-flush">
                        <p className="card-text" id="card-text-search">₹{product.price.threeMonths}</p>
                    </ul>
                </Link>
            </div>
        )
    }
}

export default (SearchProduct);