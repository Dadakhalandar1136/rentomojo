import React from "react";
import "./LoginModal.css";
import loginBackground from "../login-background.svg";
import validator from 'validator';
import { ADD_USER } from "../redux/ActionTypes";
import { connect } from "react-redux";

class LoginModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            number: "",
            errorMessage: "",
            successMessage: "",
            isDisabled: "disabled",
            valid: "",
            nextStep: false,
            name: "",
            email: "",
            error: {
                errorName: "",
                errorMail: "",
                errorSuccess: "",
            }
        }
    }

    handleNumber = (event) => {
        if (!isNaN(event.target.value) && event.target.value.length === 10) {
            this.setState({
                isDisabled: "",
            });
        } else {
            this.setState({
                isDisabled: "disabled"
            });
        }

        if (isNaN(event.target.value)) {
            this.setState({
                errorMessage: "Please Enter valid number",
                valid: "error",
            });
        } else {
            this.setState({
                number: event.target.value,
                errorMessage: "",
                valid: "Success",
            })
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();

        if (!isNaN(this.state.number) && this.state.number.length === 10) {
            // console.log("yes");
            this.setState({
                valid: "success",
                isDisabled: "",
                nextStep: true,
            })
        } else {
            // console.log("nos");
            this.setState({
                errorMessage: "Please Enter Valid Number",
                valid: "error",
                isDisabled: "disabled",
                nextStep: false,
            });
        }
    }

    handleContinue = (event) => {
        event.preventDefault();
        let errors = {};
        let count = 0;

        if (this.state.name.length === 0) {
            errors.errorName = "Please Enter your name";
        } else if (!validator.isAlpha((this.state.name).trim())) {
            errors.errorName = "This field contain only Alphabets";
        } else {
            count++;
        }

        if (this.state.email.length === 0) {
            errors.errorMail = "please Enter your valid Email";
        } else if (!validator.isEmail(this.state.email)) {
            errors.errorMail = "Example: example@gmail.com";
        } else {
            count++
        }

        if (count === 2) {
            errors.errorSuccess = "SignUp successful";
        }

        this.setState({
            error: errors,
        })

        const userData = {
            userName: this.state.name,
            userNumber: this.state.number,
            userEmail: this.state.email
        }

        this.props.signUp(userData)
    }

    render() {
        return (
            <>
                <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" id="modal-dialog-box">
                        <div className="modal-content d-flex flex-row justify-content-between">
                            <div>
                                <img src={loginBackground} className="login-image" alt="login" />
                            </div>
                            {!this.state.nextStep ?
                                <div className="right-column-box">
                                    <div className="modal-header">
                                        <div>
                                            <h1 className="modal-title fs-6 mt-4" id="exampleModalLabel">Enter your phone number to<br />Signup or Login</h1>
                                        </div>
                                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div className="d-flex flex-column justify-content-between h-75">
                                        <div className="floating-label-group m-2 pt-4 custome-input">
                                            <input type="text" className="form-control " id="exampleFormControlInput" placeholder="Enter your phone number" maxLength={10} autoComplete="off" onChange={(event) => {
                                                return this.handleNumber(event);
                                            }} />
                                            <p className="number-range">
                                                <small>{this.state.errorMessage}</small>0/10
                                            </p>
                                        </div>
                                        <div className="px-2">
                                            <div className="d-grid gap-2 col-6 mx-auto w-100">
                                                <button className={`btn btn-lg btn-block btn-danger ${this.state.isDisabled}`} type="button" onClick={this.handleSubmit}>Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> :
                                <div className="user-details-container">
                                    <h6>Just one last step</h6>
                                    <hr></hr>
                                    <div className="mb-4">
                                        <input type="text" className="form-control" id="exampleInputEmail1" placeholder="Enter your name" onChange={(event) => {
                                            return this.setState({ name: event.target.value });
                                        }}
                                        />
                                        <small>{this.state.error.errorName}</small>
                                    </div>

                                    <div className="mb-4">
                                        <input type="email" className="form-control" id="exampleInputPassword1" placeholder="Email ID" onChange={(event) => {
                                            return this.setState({ email: event.target.value });
                                        }}
                                        />
                                        <small>{this.state.error.errorMail}</small>
                                    </div>

                                    <button
                                        type="submit"
                                        className="btn btn-danger"
                                        data-bs-dismiss="modal"
                                        onClick={this.handleContinue}
                                    >
                                        Continue
                                    </button>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signUp: (userData) => {
            dispatch({
                type: ADD_USER,
                payload: userData,
            })
        }
    }
}

export default connect(null, mapDispatchToProps)(LoginModal);