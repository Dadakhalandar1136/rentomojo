import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class MyAccount extends React.Component {
    render() {
        return (
            <div className="d-flex" style={{ flex: "1", backgroundColor: "#f5f7fa" }}>
                <div className="w-25 ps-3 pt-3 bg-white ms-3 mt-3 rounded-4">
                    <div className="p-3 d-flex align-items-center">
                        <h6 className="border me-2 rounded-circle d-flex justify-content-center align-items-center"
                            style={{ width: "5rem", height: "5rem", backgroundColor: "rgb(192 161 199)", fontSize: "2rem" }}>
                            {(this.props.user?.userName).slice(0, 1)}
                        </h6>
                        <h5>{this.props.user?.userName}</h5>
                    </div>
                    <div className="p-3 d-flex justify-content-between">
                        <div className="d-flex">
                            <i className="fa-solid fa-user pe-2" style={{ color: "#55c6e2" }}></i>
                            <h6 style={{ fontWeight: "400" }}>Dashboard</h6>
                        </div>
                        <i className="fa-solid fa-angle-right pe-5"></i>
                    </div>
                    <div className="p-3 d-flex justify-content-between">
                        <div className="d-flex">
                            <i class="fa-solid fa-box-open pe-2" style={{ color: "#55c6e2", paddingLeft: "-0rem" }}></i>
                            <h6 style={{ fontWeight: "400" }}>My Subscription</h6>
                        </div>
                        <i className="fa-solid fa-angle-right pe-5"></i>
                    </div>
                    <div className="p-3 d-flex justify-content-between">
                        <div className="d-flex">
                            <i class="fa-solid fa-address-card pe-2" style={{ color: "#55c6e2" }}></i>
                            <h6 style={{ fontWeight: "400" }}>Profile</h6>
                        </div>
                        <i className="fa-solid fa-angle-right pe-5"></i>
                    </div>
                    <div className="p-3 d-flex justify-content-between">
                        <div className="d-flex">
                            <i class="fa-solid fa-wallet pe-2" style={{ color: "#55c6e2" }}></i>
                            <h6 style={{ fontWeight: "400" }}>Easy Payment</h6>
                        </div>
                        <i className="fa-solid fa-angle-right pe-5"></i>
                    </div>
                    <div className="p-3 d-flex justify-content-between">
                        <div className="d-flex">
                            <i class="fa-solid fa-user-group pe-2" style={{ color: "#55c6e2" }}></i>
                            <h6 style={{ fontWeight: "400" }}>Referral</h6>
                        </div>
                        <i className="fa-solid fa-angle-right pe-5"></i>
                    </div>
                    <div className="p-3 d-flex justify-content-between">
                        <div className="d-flex">
                            <i class="fa-solid fa-file-pen" style={{ color: "#55c6e2" }}></i>
                            <h6 style={{ fontWeight: "400" }}>Create Request</h6>
                        </div>
                        <i className="fa-solid fa-angle-right pe-5"></i>
                    </div>
                </div>
                <div>
                    <div className="bg-white ms-3 mt-3 rounded-3 border" style={{ width: "60rem" }}>
                        <div className="d-flex">
                            <img className="rounded-start" src="https://www.rentomojo.com/dashboard/_nuxt/img/banner_startRenting.1730a4a.svg" alt="banner" />
                            <div className="d-flex justify-content-between w-100 align-items-center">
                                <div>
                                    <h6 className="pt-4 ps-2 mb-0">Welcome, {this.props.user.userName}</h6>
                                    <p className="ps-2">Look's like you don't have any active orders.</p>
                                </div>
                                <div>
                                    <Link to="/">
                                        <button type="button" className="btn btn-dark me-3" style={{ width: "8rem" }}>Start Renting</button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p className="m-0 rounded-bottom p-1 ps-3 text-white" style={{fontSize: "0.9rem", backgroundColor: "#f08162"}}>Checkout offers to save more on your new lifestyle.</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.userReducer.user
    }
}

export default connect(mapStateToProps)(MyAccount);