import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class Wardrobe extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            productMatched: []
        }
    }

    componentDidMount() {
        let allProducts = window.location.href.split("/")
            .slice(4)
            .join("");

        const allProductsMatch = this.props.products.filter((product) => {
            return product.category === allProducts;
        })

        this.setState({
            productMatched: allProductsMatch
        })
    }
    render() {
        return (
            <>
                <div className="row d-flex flex-wrap">
                    {
                        this.state.productMatched.map((product) => {
                            return (
                                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                                    <div key={product.id}>
                                        <Link to={`/${product.id}`}>
                                            <img src={product.image} className="card-img-top" id="card-top-image" alt={product.title} />
                                            <div className="card-body" id="card-body-search">
                                                <h5 className="card-title" id="title-search-text">{product.title}</h5>
                                            </div>
                                            <ul className="list-group list-group-flush">
                                                <p className="card-text" id="card-text-search">₹{product.price?.threeMonths}/mo</p>
                                            </ul>
                                        </Link>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.productData.items
    }
}

export default connect(mapStateToProps)(Wardrobe);