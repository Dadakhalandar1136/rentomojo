import React from "react";

import "./SingleProduct.css";
import { connect } from "react-redux";
import { ADD_PRODUCT_TO_CART } from "../redux/ActionTypes";
import CompareTenures from "./CompareTenures";
import FooterDetails from "./FooterDetails";
import { Link } from "react-router-dom";

class SingleProduct extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            product: {},
            addedToCart: ""
        };
    }

    componentDidMount() {
        let singleProductId = window.location.href.split('/')
            .slice(3)
            .join('');
        singleProductId = Number(singleProductId);

        const sindleProduct = this.props.products.find((product) => {
            return product.id === singleProductId;
        });

        this.setState({
            product: sindleProduct
        });

    }

    handleClick = () => {
        this.props.addProductToCart(this.state.product);
        this.setState({
            addedToCart: "Product Added to Cart"
        })
        setTimeout(() => {
            this.setState({
                addedToCart: ""
            })
        }, 1000)
    }

    render() {
        return (
            <>
                <div className="main-container-single-product">
                    <div className="all-details-container">
                        <img src={this.state.product.image} alt={this.state.product.title} className="image-container-size" />
                        <div className="product-details-container">
                            <div className="option-container-one">
                                <h1 className="sub-option-text">why rent for us</h1>
                            </div>
                            <div className="option-container">
                                <h1 className="sub-option-text">Product Details</h1>
                            </div>
                        </div>
                    </div>
                    <div className="plan-price-container">
                        <h4 className="title-text-box">{this.state.product.title}</h4>
                        <div className="how-long-container">
                            <p className="mb-0">How long do you want to rent this for? (Months)</p>
                            <i className="fa-solid fa-circle-info"></i>
                        </div>
                        <div className="rent-select-container">
                            <div className="sub-rent-container">
                                <div className="monthly-container">
                                    <h6>₹{this.state.product.price?.threeMonths}</h6>
                                    <p>Monthly Rent</p>
                                </div>
                                <div className="monthly-container">
                                    <h6>₹{this.state.product.deposit}</h6>
                                    <p>Refundable Deposite</p>
                                </div>
                            </div>
                            <div className="sec-sub-container">
                                <p>Free Location</p>
                                <p>Free Upgrades</p>
                                <a href="/">View All</a>
                            </div>
                        </div>
                        <div className="compare-container">
                            <button type="button" className="btn btn-white" data-bs-toggle="modal" data-bs-target="#exampleModal5">
                                Compare All tenures
                            </button>
                        </div>
                        <div className="delivery-pin-container">
                            <div className="delivery">
                                <h6>Delivery Pin</h6>
                            </div>
                            <div className="pin">
                                <input type="text"></input>
                            </div>
                        </div>
                        <div className="buy-now-button">
                            <button type="button" className="btn btn-danger" onClick={this.handleClick}><i className="fa-solid fa-cart-shopping pe-1"></i>Book your plan</button>
                        </div>
                        <div className="d-flex justify-content-center">
                            <span className="text-center text-danger">{this.state.addedToCart}</span>
                            {
                                !this.state.addedToCart && (
                                    <Link to="/cart">
                                        <button type="button" className="btn btn-success" style={{width: "21.4rem"}}><i className="fa-solid fa-cart-shopping pe-1"></i>Go To Cart</button>
                                    </Link>
                                )
                            }
                        </div>
                    </div>
                </div>
                {
                    <CompareTenures product={this.state.product} />
                }
                <FooterDetails />
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.productData.items
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProductToCart: (product) => {
            dispatch({
                type: ADD_PRODUCT_TO_CART,
                payload: product
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleProduct);