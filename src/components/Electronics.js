import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class Electronics extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            categoryProduct: "",
            isItemFound: false,
        }
    }

    handleClick = (type) => {
        let item = this.props.items.filter((product) => {
            let categoryItem = type.toLowerCase();
            let productName = product.category.toLowerCase();

            return productName === categoryItem;
        })
        console.log(item);
        if (item.length > 0) {
            this.setState({
                categoryProduct: item,
                isItemFound: true,
            })
        } else {
            this.setState({
                isItemFound: false
            })
        }
    }
    
    render() {
        // console.log(this.state.categoryProduct);
        return (
            <div className="row d-flex flex-wrap">
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/electronics-on-rent/smartphones">
                        <img src="https://www.rentomojo.com/public/images/category/appliances-bg/smartphones_new.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("smartphones") }}>Smartphones</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/electronics-on-rent/smartphones">
                        <img src="https://www.rentomojo.com/public/images/category/appliances-bg/smart-devices-v1_new.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("smartphones") }}>Smart Devices</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/electronics-on-rent/smartphones">
                        <img src="https://www.rentomojo.com/public/images/category/appliances-bg/laptops_new_2.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("smartphones") }}>Laptops</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/electronics-on-rent/smartphones">
                        <img src="https://www.rentomojo.com/public/images/category/appliances-bg/tablets_new.jpg" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("smartphones") }}>Tablets</h5>
                        </div>
                    </Link>
                </div>
                <div className="card mb-3 mt-3 ms-3 ps-0 pe-0" id="card-container" style={{ width: "18rem" }}>
                    <Link to="/electronics-on-rent/smartphones">
                        <img src="https://www.rentomojo.com/public/images/category/electronics-category-filter/gaming-consoles/gamingConsole.png" className="card-img-top" alt="beds" id="furniture-image" />
                        <div className="card-body text-dark" id="card-body-search">
                            <h5 className="card-title" id="furniture-title" onClick={() => { this.handleClick("smartphones") }}>Gaming Consoles</h5>
                        </div>
                    </Link>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.productData.items,
    }
}

export default connect(mapStateToProps)(Electronics);