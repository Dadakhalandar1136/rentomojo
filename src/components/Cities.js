import React from "react";
import "./Cities.css";

class Cities extends React.Component {
    render() {
        return (
            <div className="modal fade" id="exampleModal1" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-xl border bg-white p-2 rounded-1 mt-5">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <form className="form-inline">
                                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                            </form>
                            <div className="d-flex flex-wrap p-3 text-center text-secondary">
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://www.shutterstock.com/image-vector/indian-city-icon-bangalorevidhan-soudha-600w-1455972629.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Banglore</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://www.shutterstock.com/image-vector/indian-city-icon-mumbaigateway-india-600w-1455972620.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Mumbai</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://www.shutterstock.com/image-vector/beautiful-indian-city-icon-pune-600w-2079901474.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Pune</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://www.shutterstock.com/image-vector/indian-city-icon-delhiindia-gate-600w-1455972353.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Delhi</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://www.shutterstock.com/image-vector/vector-black-icon-authority-600w-1912014427.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Noida</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://www.shutterstock.com/image-vector/gurgaon-city-tower-building-landmark-600w-2147300109.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Gurgaon</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://www.shutterstock.com/image-vector/india-charminar-icon-logo-600w-1169678608.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Hydrabad</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://www.shutterstock.com/image-vector/indian-city-icon-chennaieric-schmidt-600w-2237619569.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Chennai</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://www.shutterstock.com/image-vector/ahmedabads-iconic-jhulta-minar-sidi-600w-1880161501.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Ahemadaba</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://www.shutterstock.com/image-vector/mysore-palace-indian-famous-iconic-600w-2056702475.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Mysore</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://www.shutterstock.com/image-vector/jaipurs-famous-hawa-mahal-line-600w-1879556095.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Jaipur</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle pt-2" src="https://ohlocal-media.s3.amazonaws.com/media/city_images/fbd_CITY_ICONS-02.png" alt="one" style={{ width: "6rem" }} />
                                    <p>Faridabad</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle pt-2" src="https://ohlocal-media.s3.amazonaws.com/media/city_images/CITY_ICONS_ghaziabad-03.png" alt="one" style={{ width: "6rem" }} />
                                    <p>Ghaziabad</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle p-2" src="https://c8.alamy.com/comp/2JTG2H2/gandhinagar-akshardham-temple-icon-illustration-as-eps-10-file-2JTG2H2.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Ghandinagar</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://cdn.iconscout.com/icon/free/png-256/free-chandigarh-1-161356.png" alt="one" style={{ width: "6rem" }} />
                                    <p>Chandigarh</p>
                                </div>
                                <div className="cities-box">
                                    <img className="rounded-circle" src="https://www.shutterstock.com/image-vector/victoria-memorial-indian-famous-iconic-600w-2056702427.jpg" alt="one" style={{ width: "6rem" }} />
                                    <p>Kolkata</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Cities;