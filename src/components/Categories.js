import React from "react";
import packageIcon from "../assets/package-icon.png";
import furnitureIconBed from "../assets/furniture-icon-bad.jpg";
import mobilePhone from "../assets/mobile-phone.png";
import appliances from "../assets/appliances.png";
import bikes from "../assets/bikes.svg";
import threadMill from "../assets/threadmill.png";
import essentials from "../assets/essentials.svg";
import "./Categories.css";
import { Link } from "react-router-dom";

class Categories extends React.Component {
    render() {
        return (
            <div className="row row-cols-1 row-cols-md-6 g-3 w-75">
                <div className="col">
                    <div className="card d-flex align-items-center">
                        <img src={packageIcon} className="img" alt="..." />
                        <div className="card-body">
                            <h5 className="card-title">Packages</h5>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <Link to="/furniture-on-rent">
                        <div className="card d-flex align-items-center">
                            <img src={furnitureIconBed} className="img" alt="..." />
                            <div className="card-body">
                                <h5 className="card-title">Furniture</h5>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className="col">
                    <Link to="/appliances-on-rent">
                        <div className="card d-flex align-items-center">
                            <img src={appliances} className="img" alt="..." />
                            <div className="card-body">
                                <h5 className="card-title">Appliances</h5>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className="col">
                    <Link to="/electronics-on-rent">
                        <div className="card d-flex align-items-center">
                            <img src={mobilePhone} className="img" alt="..." />
                            <div className="card-body ">
                                <h5 className="card-title">Electronics</h5>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className="col">
                    <div className="card d-flex align-items-center">
                        <img src={bikes} className="img" alt="..." />
                        <div className="card-body ">
                            <h5 className="card-title">Bikes</h5>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <Link to="/fitness-on-rent">
                        <div className="card d-flex align-items-center">
                            <img src={threadMill} className="img" alt="..." />
                            <div className="card-body ">
                                <h5 className="card-title">Fitness</h5>
                            </div>
                        </div>
                    </Link>
                </div>
                <div className="col">
                    <div className="card d-flex align-items-center p-0">
                        <img src={essentials} className="img" alt="..." />
                        <div className="card-body ">
                            <h5 className="card-title">WFH Essentials</h5>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Categories;