import { configureStore } from '@reduxjs/toolkit';

import productData from './reducer/ProductData';
import cartReducer from './reducer/CartReducer';
import userReducer from './reducer/UserReducer';

const reducer = {
    productData,
    cartReducer,
    userReducer
};

export default configureStore({
    reducer
});
