import { ADD_PRODUCT_TO_CART, INCREAMENT_QUANTITY, DECREMENT_QUANTITY, DELETE_PRODUCT_FROM_CART } from "../ActionTypes";

const initialState = {
    list: [],
}

export default function cartReducer(state = initialState, action) {
    switch (action.type) {

        case ADD_PRODUCT_TO_CART: {
            return {
                ...state,
                list: [
                    ...state.list,
                    action.payload
                ]
            }
        }

        case INCREAMENT_QUANTITY: {
            return {
                ...state,
                list: state.list.map((presentProduct) => {
                    if (presentProduct.id === action.payload) {
                        const productModified = {
                            ...presentProduct,
                            quantity: presentProduct.quantity + 1
                        };
                        return productModified;
                    } else {
                        return presentProduct;
                    }
                })
            }
        }

        case DECREMENT_QUANTITY: {
            return {
                ...state,
                list: state.list.map((presentProduct) => {
                    if (presentProduct.id === action.payload) {
                        const productModified = {
                            ...presentProduct,
                            quantity: presentProduct.quantity - 1
                        };
                        return productModified;
                    } else {
                        return presentProduct;
                    }
                })
            }
        }

        case DELETE_PRODUCT_FROM_CART: {
            return {
                ...state,
                list: state.list.filter((presentProduct) => {
                    return presentProduct.id !== action.payload;
                })
            }
        }

        default: {
            return state;
        }
    }
}