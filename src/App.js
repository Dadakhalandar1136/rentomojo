import React from 'react';
import './App.css';
import Header from './components/Header';
import ImageSlider from './components/ImageSlider';
import Categories from './components/Categories';
import ProductList from './components/ProductsList';
import Footer from './components/Footer';
import RentingDetails from './components/RentingDetails';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import SearchProduct from './components/SearchProduct';
import { connect } from 'react-redux';
import MostPopularProducts from './components/MostPopularProducts';
import SingleProduct from './components/SingleProduct';
import Cart from './components/Cart';
import Furniture from './components/Furnitures';
import Beds from './components/Categories/Furniture/Beds';
import Wardrobe from './components/Categories/Furniture/Wardrobe.js';
import Mattresses from './components/Categories/Furniture/Mattresses.js';
import Appliances from './components/Appliances';
import WashingMachine from './components/Categories/Appliances/WashingMachine';
import Electronics from './components/Electronics';
import SmartPhones from './components/Categories/Electronics/SmartPhones';
import FooterDetails from './components/FooterDetails';
import Fitness from './components/Fitness';
import Threadmills from './components/Categories/Fitness/Threadmills';
import Crosstrainers from './components/Categories/Fitness/Crosstrainers';
import MyAccount from './components/MyAccount';


class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      product: "",
      isItemFound: false,
    }
  }

  handleSearch = (event) => {
    let item = this.props.items.filter((product) => {
      let name = event.target.value.toLowerCase();
      let productName = product.title.toLowerCase();

      return productName.includes(name);
    });
    if (item.length > 0) {
      this.setState({
        value: event.target.value,
        product: item,
        isItemFound: true,
      })
    } else {
      this.setState({
        isItemFound: false,
      })
    }
  }

  render() {
    return (
      <Router>
        <>
          <div className='d-flex flex-column' id='main-body'>
            <div className=' bg-white' id='header-container'>
              <Header Products={this.props.items}
                handleChangeValue={this.handleSearch} />
            </div>
            <Routes>
              <Route path="/" element={
                <>
                  <div className='d-flex justify-content-center'>
                    <ImageSlider />
                  </div>
                  <div className='categories-container'>
                    <Categories />
                  </div>
                  <div className='product-container d-flex justify-content-center pt-5 w-75'>
                    <ProductList />
                  </div>
                  <div className='renting-details'>
                    <RentingDetails />
                  </div>
                  <div>
                    <FooterDetails />
                  </div>
                </>
              } />
              <Route path='/searched' element={
                <>
                  {this.state.isItemFound === true && this.state.value !== "" &&
                    <>
                      <div className="container common-container" id='products-container'>
                        <div className="row d-flex flex-wrap ms-0">
                          {this.state.product.map((product) => {
                            return (
                              <SearchProduct
                                item={product}
                                key={product.id}
                              />
                            )
                          })}
                        </div>
                      </div>
                    </>
                  }
                  {this.state.value === "" &&
                    <MostPopularProducts />
                  }
                  {this.state.value !== "" && this.state.isItemFound === false &&
                    <>
                      <div className='h1 text-center'>No Item found </div>
                    </>
                  }

                </>
              }
              />
              <Route path='/:id' element={<SingleProduct />} />
              <Route path='/cart' element={<Cart />} />

              <Route path='/furniture-on-rent' element={<Furniture />} />
              <Route path='/furniture-on-rent/bed' element={<Beds />} />
              <Route path='/furniture-on-rent/wardrobe' element={<Wardrobe />} />
              <Route path='/furniture-on-rent/mattresses' element={<Mattresses />} />

              <Route path='/appliances-on-rent' element={<Appliances />} />
              <Route path='/appliances-on-rent/washing-machine' element={<WashingMachine />} />

              <Route path='/electronics-on-rent' element={<Electronics />} />
              <Route path='/electronics-on-rent/smartphones' element={<SmartPhones />} />

              <Route path='/fitness-on-rent' element={<Fitness />} />
              <Route path='/fitness-on-rent/threadmills' element={<Threadmills />} />
              <Route path='/fitness-on-rent/crosstrainers' element={<Crosstrainers />} />

              <Route path='/myaccount' element={<MyAccount />} />
            </Routes>
            <div>
              <Footer />
            </div>
          </div>
        </>
      </Router>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    items: state.productData.items,
  }
}

export default connect(mapStateToProps)(App);
